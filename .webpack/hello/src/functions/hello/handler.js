/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ 978:
/***/ ((__unused_webpack_module, exports) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.formatJSONResponse = void 0;
const formatJSONResponse = (response, statusCode = 200) => {
    return {
        statusCode,
        body: JSON.stringify(response)
    };
};
exports.formatJSONResponse = formatJSONResponse;


/***/ }),

/***/ 940:
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.middyfy = void 0;
const core_1 = __importDefault(__webpack_require__(862));
const http_json_body_parser_1 = __importDefault(__webpack_require__(582));
const input_output_logger_1 = __importDefault(__webpack_require__(655));
const http_header_normalizer_1 = __importDefault(__webpack_require__(451));
const http_event_normalizer_1 = __importDefault(__webpack_require__(932));
function middyfy(handler) {
    return (0, core_1.default)(handler)
        .use((0, http_json_body_parser_1.default)())
        .use((0, http_header_normalizer_1.default)())
        .use((0, http_event_normalizer_1.default)())
        .use((0, input_output_logger_1.default)());
}
exports.middyfy = middyfy;


/***/ }),

/***/ 862:
/***/ ((module) => {

module.exports = require("@middy/core");

/***/ }),

/***/ 932:
/***/ ((module) => {

module.exports = require("@middy/http-event-normalizer");

/***/ }),

/***/ 451:
/***/ ((module) => {

module.exports = require("@middy/http-header-normalizer");

/***/ }),

/***/ 582:
/***/ ((module) => {

module.exports = require("@middy/http-json-body-parser");

/***/ }),

/***/ 655:
/***/ ((module) => {

module.exports = require("@middy/input-output-logger");

/***/ }),

/***/ 725:
/***/ ((module) => {

module.exports = require("@social-solutions/ssg-logging");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {
var exports = __webpack_exports__;

Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.main = void 0;
const apiGateway_1 = __webpack_require__(978);
const lambda_1 = __webpack_require__(940);
const ssg_logging_1 = __webpack_require__(725);
const hello = async (event) => {
    const logger = ssg_logging_1.SsgLoggerFactory.createLogger(event.requestContext.requestId);
    logger.logInfo({ id: 13245 }, "testing the logger. LOG_TRANSPORT_TYPE={@type}", process.env.LOG_TRANSPORT_TYPE);
    return (0, apiGateway_1.formatJSONResponse)({
        message: `Hello ${event.body.name}, welcome to the exciting Serverless world!`,
        event,
    });
};
exports.main = (0, lambda_1.middyfy)(hello);

})();

var __webpack_export_target__ = exports;
for(var i in __webpack_exports__) __webpack_export_target__[i] = __webpack_exports__[i];
if(__webpack_exports__.__esModule) Object.defineProperty(__webpack_export_target__, "__esModule", { value: true });
/******/ })()
;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3JjL2Z1bmN0aW9ucy9oZWxsby9oYW5kbGVyLmpzIiwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFRTyxNQUFNLGtCQUFrQixHQUFHLENBQUMsUUFBaUMsRUFBRSxVQUFVLEdBQUcsR0FBRyxFQUFFLEVBQUU7SUFDdEYsT0FBTztRQUNILFVBQVU7UUFDVixJQUFJLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUM7S0FDakM7QUFDTCxDQUFDO0FBTFksMEJBQWtCLHNCQUs5Qjs7Ozs7Ozs7Ozs7Ozs7QUNiRCx5REFBK0I7QUFDL0IsMEVBQThEO0FBQzlELHdFQUEwRDtBQUMxRCwyRUFBZ0U7QUFDaEUsMEVBQThEO0FBTTlELFNBQWdCLE9BQU8sQ0FBOEIsT0FBdUM7SUFDeEYsT0FBTyxrQkFBSyxFQUFDLE9BQU8sQ0FBQztTQUNoQixHQUFHLENBQUMsbUNBQW1CLEdBQUUsQ0FBQztTQUMxQixHQUFHLENBQUMsb0NBQW9CLEdBQUUsQ0FBQztTQUMzQixHQUFHLENBQUMsbUNBQW1CLEdBQUUsQ0FBQztTQUMxQixHQUFHLENBQUMsaUNBQWlCLEdBQUUsQ0FBQyxDQUN4QjtBQUNULENBQUM7QUFQRCwwQkFPQzs7Ozs7Ozs7QUNqQkQ7Ozs7Ozs7QUNBQTs7Ozs7OztBQ0FBOzs7Ozs7O0FDQUE7Ozs7Ozs7QUNBQTs7Ozs7OztBQ0FBOzs7Ozs7VUNBQTtVQUNBOztVQUVBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBOztVQUVBO1VBQ0E7O1VBRUE7VUFDQTtVQUNBOzs7Ozs7Ozs7O0FDckJBLDhDQUFzRDtBQUN0RCwwQ0FBdUM7QUFDdkMsK0NBQTBFO0FBSTFFLE1BQU0sS0FBSyxHQUEwRCxLQUFLLEVBQUUsS0FBSyxFQUFFLEVBQUU7SUFDakYsTUFBTSxNQUFNLEdBQUcsOEJBQWdCLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLENBQUM7SUFDN0UsTUFBTSxDQUFDLE9BQU8sQ0FBQyxFQUFFLEVBQUUsRUFBRSxLQUFLLEVBQWEsRUFBRSxnREFBZ0QsRUFBRSxPQUFPLENBQUMsR0FBRyxDQUFDLGtCQUFrQixDQUFDLENBQUM7SUFDM0gsT0FBTyxtQ0FBa0IsRUFBQztRQUN0QixPQUFPLEVBQUUsU0FBUyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksNkNBQTZDO1FBQzlFLEtBQUs7S0FDUixDQUFDLENBQUM7QUFDUCxDQUFDO0FBRVksWUFBSSxHQUFHLG9CQUFPLEVBQUMsS0FBSyxDQUFDLENBQUMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9tam91cmFyZC9naXRfcmVwb3MvYml0YnVja2V0Lm9yZy9tYXR0am91cmFyZHNzZy9sYWIvbGFtYmRhLWxvZ2dlci1kZW1vL3NyYy9saWJzL2FwaUdhdGV3YXkudHMiLCIvVXNlcnMvbWpvdXJhcmQvZ2l0X3JlcG9zL2JpdGJ1Y2tldC5vcmcvbWF0dGpvdXJhcmRzc2cvbGFiL2xhbWJkYS1sb2dnZXItZGVtby9zcmMvbGlicy9sYW1iZGEudHMiLCJleHRlcm5hbCBjb21tb25qcyBcIkBtaWRkeS9jb3JlXCIiLCJleHRlcm5hbCBjb21tb25qcyBcIkBtaWRkeS9odHRwLWV2ZW50LW5vcm1hbGl6ZXJcIiIsImV4dGVybmFsIGNvbW1vbmpzIFwiQG1pZGR5L2h0dHAtaGVhZGVyLW5vcm1hbGl6ZXJcIiIsImV4dGVybmFsIGNvbW1vbmpzIFwiQG1pZGR5L2h0dHAtanNvbi1ib2R5LXBhcnNlclwiIiwiZXh0ZXJuYWwgY29tbW9uanMgXCJAbWlkZHkvaW5wdXQtb3V0cHV0LWxvZ2dlclwiIiwiZXh0ZXJuYWwgY29tbW9uanMgXCJAc29jaWFsLXNvbHV0aW9ucy9zc2ctbG9nZ2luZ1wiIiwid2VicGFjay9ib290c3RyYXAiLCIvVXNlcnMvbWpvdXJhcmQvZ2l0X3JlcG9zL2JpdGJ1Y2tldC5vcmcvbWF0dGpvdXJhcmRzc2cvbGFiL2xhbWJkYS1sb2dnZXItZGVtby9zcmMvZnVuY3Rpb25zL2hlbGxvL2hhbmRsZXIudHMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHR5cGUgeyBBUElHYXRld2F5UHJveHlFdmVudCwgQVBJR2F0ZXdheVByb3h5UmVzdWx0LCBIYW5kbGVyIH0gZnJvbSBcImF3cy1sYW1iZGFcIlxuaW1wb3J0IHR5cGUgeyBGcm9tU2NoZW1hIH0gZnJvbSBcImpzb24tc2NoZW1hLXRvLXRzXCI7XG5cbnR5cGUgVmFsaWRhdGVkQVBJR2F0ZXdheVBPU1RQcm94eUV2ZW50PFM+ID0gT21pdDxBUElHYXRld2F5UHJveHlFdmVudCwgJ2JvZHknPiAmIHsgYm9keTogRnJvbVNjaGVtYTxTPiB9XG50eXBlIFZhbGlkYXRlZEFQSUdhdGV3YXlHRVRQcm94eUV2ZW50PFM+ID0gT21pdDxBUElHYXRld2F5UHJveHlFdmVudCwgJ3F1ZXJ5U3RyaW5nUGFyYW1ldGVycyc+ICYgeyBxdWVyeVN0cmluZ1BhcmFtZXRlcnM6IEZyb21TY2hlbWE8Uz4gfVxuZXhwb3J0IHR5cGUgVmFsaWRhdGVkRXZlbnRBUElHYXRld2F5UE9TVFByb3h5RXZlbnQ8Uz4gPSBIYW5kbGVyPFZhbGlkYXRlZEFQSUdhdGV3YXlQT1NUUHJveHlFdmVudDxTPiwgQVBJR2F0ZXdheVByb3h5UmVzdWx0PlxuZXhwb3J0IHR5cGUgVmFsaWRhdGVkRXZlbnRBUElHYXRld2F5R0VUUHJveHlFdmVudDxTPiA9IEhhbmRsZXI8VmFsaWRhdGVkQVBJR2F0ZXdheUdFVFByb3h5RXZlbnQ8Uz4sIEFQSUdhdGV3YXlQcm94eVJlc3VsdD5cblxuZXhwb3J0IGNvbnN0IGZvcm1hdEpTT05SZXNwb25zZSA9IChyZXNwb25zZTogUmVjb3JkPHN0cmluZywgdW5rbm93bj4sIHN0YXR1c0NvZGUgPSAyMDApID0+IHtcbiAgICByZXR1cm4ge1xuICAgICAgICBzdGF0dXNDb2RlLFxuICAgICAgICBib2R5OiBKU09OLnN0cmluZ2lmeShyZXNwb25zZSlcbiAgICB9XG59XG4iLCJpbXBvcnQgbWlkZHkgZnJvbSBcIkBtaWRkeS9jb3JlXCJcbmltcG9ydCBtaWRkeUpzb25Cb2R5UGFyc2VyIGZyb20gXCJAbWlkZHkvaHR0cC1qc29uLWJvZHktcGFyc2VyXCJcbmltcG9ydCBpbnB1dE91dHB1dExvZ2dlciBmcm9tICdAbWlkZHkvaW5wdXQtb3V0cHV0LWxvZ2dlcidcbmltcG9ydCBodHRwSGVhZGVyTm9ybWFsaXplciBmcm9tICdAbWlkZHkvaHR0cC1oZWFkZXItbm9ybWFsaXplcidcbmltcG9ydCBodHRwRXZlbnROb3JtYWxpemVyIGZyb20gJ0BtaWRkeS9odHRwLWV2ZW50LW5vcm1hbGl6ZXInXG5pbXBvcnQgeyBIYW5kbGVyIGFzIExhbWJkYUhhbmRsZXIgfSBmcm9tICdhd3MtbGFtYmRhJ1xuXG5cbi8vIG5lZWQgdG8gYWxsb3cgYW55IGJlY2F1c2Ugb2YgdGhlIGludGVyZmFjZSBkZWZpbmVkIGJ5IG1pZGR5anNcbi8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSBAdHlwZXNjcmlwdC1lc2xpbnQvbm8tZXhwbGljaXQtYW55XG5leHBvcnQgZnVuY3Rpb24gbWlkZHlmeTxURXZlbnQgPSBhbnksIFRSZXN1bHQgPSBhbnk+KGhhbmRsZXI6IExhbWJkYUhhbmRsZXI8VEV2ZW50LCBUUmVzdWx0Pikge1xuICAgIHJldHVybiBtaWRkeShoYW5kbGVyKVxuICAgICAgICAudXNlKG1pZGR5SnNvbkJvZHlQYXJzZXIoKSlcbiAgICAgICAgLnVzZShodHRwSGVhZGVyTm9ybWFsaXplcigpKVxuICAgICAgICAudXNlKGh0dHBFdmVudE5vcm1hbGl6ZXIoKSlcbiAgICAgICAgLnVzZShpbnB1dE91dHB1dExvZ2dlcigpKVxuICAgICAgICA7XG59XG4iLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJAbWlkZHkvY29yZVwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJAbWlkZHkvaHR0cC1ldmVudC1ub3JtYWxpemVyXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIkBtaWRkeS9odHRwLWhlYWRlci1ub3JtYWxpemVyXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIkBtaWRkeS9odHRwLWpzb24tYm9keS1wYXJzZXJcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiQG1pZGR5L2lucHV0LW91dHB1dC1sb2dnZXJcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiQHNvY2lhbC1zb2x1dGlvbnMvc3NnLWxvZ2dpbmdcIik7IiwiLy8gVGhlIG1vZHVsZSBjYWNoZVxudmFyIF9fd2VicGFja19tb2R1bGVfY2FjaGVfXyA9IHt9O1xuXG4vLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcblx0dmFyIGNhY2hlZE1vZHVsZSA9IF9fd2VicGFja19tb2R1bGVfY2FjaGVfX1ttb2R1bGVJZF07XG5cdGlmIChjYWNoZWRNb2R1bGUgIT09IHVuZGVmaW5lZCkge1xuXHRcdHJldHVybiBjYWNoZWRNb2R1bGUuZXhwb3J0cztcblx0fVxuXHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuXHR2YXIgbW9kdWxlID0gX193ZWJwYWNrX21vZHVsZV9jYWNoZV9fW21vZHVsZUlkXSA9IHtcblx0XHQvLyBubyBtb2R1bGUuaWQgbmVlZGVkXG5cdFx0Ly8gbm8gbW9kdWxlLmxvYWRlZCBuZWVkZWRcblx0XHRleHBvcnRzOiB7fVxuXHR9O1xuXG5cdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuXHRfX3dlYnBhY2tfbW9kdWxlc19fW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuXHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuXHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG59XG5cbiIsImltcG9ydCB0eXBlIHsgVmFsaWRhdGVkRXZlbnRBUElHYXRld2F5UE9TVFByb3h5RXZlbnQgfSBmcm9tICdAbGlicy9hcGlHYXRld2F5JztcbmltcG9ydCB7IGZvcm1hdEpTT05SZXNwb25zZSB9IGZyb20gJ0BsaWJzL2FwaUdhdGV3YXknO1xuaW1wb3J0IHsgbWlkZHlmeSB9IGZyb20gJ0BsaWJzL2xhbWJkYSc7XG5pbXBvcnQgeyBFdmVudElkLCBTc2dMb2dnZXJGYWN0b3J5IH0gZnJvbSAnQHNvY2lhbC1zb2x1dGlvbnMvc3NnLWxvZ2dpbmcnO1xuXG5pbXBvcnQgc2NoZW1hIGZyb20gJy4vc2NoZW1hJztcblxuY29uc3QgaGVsbG86IFZhbGlkYXRlZEV2ZW50QVBJR2F0ZXdheVBPU1RQcm94eUV2ZW50PHR5cGVvZiBzY2hlbWE+ID0gYXN5bmMgKGV2ZW50KSA9PiB7XG4gICAgY29uc3QgbG9nZ2VyID0gU3NnTG9nZ2VyRmFjdG9yeS5jcmVhdGVMb2dnZXIoZXZlbnQucmVxdWVzdENvbnRleHQucmVxdWVzdElkKTtcbiAgICBsb2dnZXIubG9nSW5mbyh7IGlkOiAxMzI0NSB9IGFzIEV2ZW50SWQsIFwidGVzdGluZyB0aGUgbG9nZ2VyLiBMT0dfVFJBTlNQT1JUX1RZUEU9e0B0eXBlfVwiLCBwcm9jZXNzLmVudi5MT0dfVFJBTlNQT1JUX1RZUEUpO1xuICAgIHJldHVybiBmb3JtYXRKU09OUmVzcG9uc2Uoe1xuICAgICAgICBtZXNzYWdlOiBgSGVsbG8gJHtldmVudC5ib2R5Lm5hbWV9LCB3ZWxjb21lIHRvIHRoZSBleGNpdGluZyBTZXJ2ZXJsZXNzIHdvcmxkIWAsXG4gICAgICAgIGV2ZW50LFxuICAgIH0pO1xufVxuXG5leHBvcnQgY29uc3QgbWFpbiA9IG1pZGR5ZnkoaGVsbG8pO1xuIl0sIm5hbWVzIjpbXSwic291cmNlUm9vdCI6IiJ9