# lambda-logger-demo

A Serverless Framework project demo in typescript showing the bug in the https://bitbucket.org/social-solutions/ssg-logging/src/develop/ library.
The library is supposed to not create file logs when passing in 2 or 4 as the LOG_TRANSPORT_TYPE environment variable.

This repo will create a typescript lambda function in AWS with the LOG_TRANSPORT_TYPE set to '2'. The lambda can then be called with 
```
curl -X POST -H "Content-Type: application/json" -d '{"name": "joe"}' <url>
```
and it will return an Internal Server Error due to the logger creating a file in the lambda environment (see cloudwatch logs of the lambda for details).

SsgLogger is initialized and used in `src/functions/hello/handler.ts`.

LOG_TRANSPORT_TYPE is set in the configs in `serverless.ts:serverlessConfiguration.provider.environment.LOG_TRANSPORT_TYPE` but it's recommended you modify it through the AWS console for speed.

## Deployment steps

Install the node modules and then run the deploy script (Note: you'll need the environment variable NPM_TOKEN in order to install the ssg logging library):
```
npm i
npm run deploy
```
