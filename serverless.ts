import type { AWS } from '@serverless/typescript';


import hello from '@functions/hello';
import goodbye from '@functions/goodbye';

enum TransportType {
    File = 1,
    Console = 2,
    Both = 3,
    None = 4
}

const useWebpack = process.env.USE_WEBPACK;
const builderPlugin = useWebpack ? 'serverless-webpack' : 'serverless-esbuild';

const serverlessConfiguration: AWS = {
    service: 'lambda-logger-demo',
    frameworkVersion: '2',
    package: {
        individually: true
    },
    custom: {
        esbuild: {
            bundle: true,
            minify: false,
            sourcemap: true,
            exclude: ['aws-sdk'],
            target: 'node14',
            define: { 'require.resolve': undefined },
            platform: 'node',
        },
        webpack: {
            webpackConfig: "webpack.config.js",
            includeModules: {
                forceExclude: [
                    'aws-sdk'
                ],
                // forceInclude: [
                //     'dotenv'
                // ]
            },
            packager: "npm",
            excludeFiles: 'src/**/*.spec.ts',
            keepOutputDirectory: true
        }
    },
    plugins: [
        'serverless-prune-plugin',
        builderPlugin,
    ],
    provider: {
        name: 'aws',
        runtime: 'nodejs14.x',
        region: 'us-east-2',
        apiGateway: {
            minimumCompressionSize: 1024,
            shouldStartNameWithService: true,
        },
        environment: {
            AWS_NODEJS_CONNECTION_REUSE_ENABLED: '1',
            LOG_TRANSPORT_TYPE: TransportType.Console.toString(),
            NODE_OPTIONS: '--enable-source-maps --stack-trace-limit=1000',
        },
        lambdaHashingVersion: '20201221',
    },
    // import the function via paths
    functions: { hello, goodbye },
};

module.exports = serverlessConfiguration;
