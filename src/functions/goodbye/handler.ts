import type { ValidatedEventAPIGatewayPOSTProxyEvent } from '@libs/apiGateway';
import { formatJSONResponse } from '@libs/apiGateway';
import { middyfy } from '@libs/lambda';
import { EventId, SsgLoggerFactory } from '@social-solutions/ssg-logging';

import schema from './schema';

const hello: ValidatedEventAPIGatewayPOSTProxyEvent<typeof schema> = async (event) => {
    const logger = SsgLoggerFactory.createLogger(event.requestContext.requestId);
    const id = event.body.id;
    logger.logInfo({ id } as EventId, "testing the logger. LOG_TRANSPORT_TYPE={@type}", process.env.LOG_TRANSPORT_TYPE);
    return formatJSONResponse({
        message: `Goodbye to event ${id}.`,
    });
}

export const main = middyfy(hello);
