import type { ValidatedEventAPIGatewayPOSTProxyEvent } from '@libs/apiGateway';
import { formatJSONResponse } from '@libs/apiGateway';
import { middyfy } from '@libs/lambda';
import { EventId, SsgLoggerFactory } from '@social-solutions/ssg-logging';

import schema from './schema';

const hello: ValidatedEventAPIGatewayPOSTProxyEvent<typeof schema> = async (event) => {
    const logger = SsgLoggerFactory.createLogger(event.requestContext.requestId);
    logger.logInfo({ id: 13245 } as EventId, "testing the logger. LOG_TRANSPORT_TYPE={@type}", process.env.LOG_TRANSPORT_TYPE);
    return formatJSONResponse({
        message: `Hello ${event.body.name}, welcome to the exciting Serverless world!`,
        event,
    });
}

export const main = middyfy(hello);
