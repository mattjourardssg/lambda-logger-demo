import type { APIGatewayProxyEvent, APIGatewayProxyResult, Handler } from "aws-lambda"
import type { FromSchema } from "json-schema-to-ts";

type ValidatedAPIGatewayPOSTProxyEvent<S> = Omit<APIGatewayProxyEvent, 'body'> & { body: FromSchema<S> }
type ValidatedAPIGatewayGETProxyEvent<S> = Omit<APIGatewayProxyEvent, 'queryStringParameters'> & { queryStringParameters: FromSchema<S> }
export type ValidatedEventAPIGatewayPOSTProxyEvent<S> = Handler<ValidatedAPIGatewayPOSTProxyEvent<S>, APIGatewayProxyResult>
export type ValidatedEventAPIGatewayGETProxyEvent<S> = Handler<ValidatedAPIGatewayGETProxyEvent<S>, APIGatewayProxyResult>

export const formatJSONResponse = (response: Record<string, unknown>, statusCode = 200) => {
    return {
        statusCode,
        body: JSON.stringify(response)
    }
}
