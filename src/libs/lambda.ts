import middy from "@middy/core"
import middyJsonBodyParser from "@middy/http-json-body-parser"
import inputOutputLogger from '@middy/input-output-logger'
import httpHeaderNormalizer from '@middy/http-header-normalizer'
import httpEventNormalizer from '@middy/http-event-normalizer'
import { Handler as LambdaHandler } from 'aws-lambda'


// need to allow any because of the interface defined by middyjs
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function middyfy<TEvent = any, TResult = any>(handler: LambdaHandler<TEvent, TResult>) {
    return middy(handler)
        .use(middyJsonBodyParser())
        .use(httpHeaderNormalizer())
        .use(httpEventNormalizer())
        .use(inputOutputLogger())
        ;
}
