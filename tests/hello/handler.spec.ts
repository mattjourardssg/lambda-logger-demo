import { SsgLoggerFactory } from '@social-solutions/ssg-logging';
import 'mocha';


describe('handler', function () {
    it('should create a file when passing in 1', function () {
        // this is not a good automated test and requires a human to look at what's in the repo after running it and also the output of the test in the console
        const tempVal = process.env.LOG_TRANSPORT_TYPE;

        process.env.LOG_TRANSPORT_TYPE = "1";
        let logger = SsgLoggerFactory.createLogger('temp');
        logger.logInfo({ id: 12345 }, 'whatever, transport type = {@type}', process.env.LOG_TRANSPORT_TYPE);
        process.env.LOG_TRANSPORT_TYPE = "2";
        logger = SsgLoggerFactory.createLogger('temp2');
        logger.logInfo({ id: 12346 }, 'whatever, transport type = {@type}', process.env.LOG_TRANSPORT_TYPE);
        process.env.LOG_TRANSPORT_TYPE = tempVal;

    })
})