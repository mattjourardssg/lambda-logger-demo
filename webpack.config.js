const path = require('path');
const slsw = require('serverless-webpack');
const nodeExternals = require('webpack-node-externals');
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');

/**
 * Note: slsw is only populated when webpack is initialized from the serverless framework
 * i.e. `serverless package` is called
 */

/* These resolve aliases are a left-over attempt for getting TSOA to play nice with webpack. They'll be needed later and they don't hurt build times so I kept them in. */
const resolveAlias = {
    'handlebars/runtime': 'handlebars/dist/cjs/handlebars.runtime',
    'handlebars': 'handlebars/dist/cjs/handlebars.runtime',
};

/* this path is also required by the serverless-webpack plugin */
const outputPath = path.resolve(__dirname, '.webpack');
module.exports = {
    target: 'node',
    mode: slsw.lib.webpack.isLocal ? 'development' : 'production',
    devtool: slsw.lib.webpack.isLocal ? 'eval-cheap-module-source-map' : 'inline-source-map',
    entry: slsw.lib.entries,
    externals: [
        nodeExternals()
    ],
    externalsPresets: { node: true },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                // exclude: /node_modules/,
            },
            {
                test: /\.json$/,
                loader: 'json-loader',
                exclude: /.*node_modules.*/,
            }
        ],
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js', '.jsx', '.json'],
        alias: resolveAlias,
        plugins: [new TsconfigPathsPlugin({})]
    },
    output: {
        libraryTarget: 'commonjs',
        filename: '[name].js',
        path: outputPath,
        clean: true,
        devtoolModuleFilenameTemplate: '[absolute-resource-path]'
    },
    optimization: {
        minimize: false,
        innerGraph: true
    },
    performance: {
        hints: false
    },
};
